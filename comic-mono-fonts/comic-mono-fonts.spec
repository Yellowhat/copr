%global foundry        Comic Mono
%global fontlicense    MIT
%global fontlicenses   LICENSE
%global fontfamily     Comic Mono
%global fontsummary    A legible monospace font
%global fonts          *.ttf
%global fontdescription %{expand:
A legible monospace font... the very typeface you’ve been trained to recognize since childhood.
}

Version:        1.0
Release:        0%{?dist}
URL:            https://dtinth.github.io/comic-mono-font
Source0:        https://dtinth.github.io/comic-mono-font/ComicMono.ttf
Source1:        https://dtinth.github.io/comic-mono-font/ComicMono-Bold.ttf
Source2:        https://raw.githubusercontent.com/dtinth/comic-mono-font/master/LICENSE

%fontpkg


%prep
%setup -cT
cp %{SOURCE0} comic-mono.ttf
cp %{SOURCE1} comic-mono-bold.ttf
cp %{SOURCE2} LICENSE


%install
%fontinstall


%files
%fontfiles
