%global COREUTILS_VER 9.5

Name:           advcpmv
Version:        0.9
Release:        1%{?dist}
Summary:        A patch for GNU Core Utilities cp, mv to add progress bars
License:        GPLv3+
URL:            https://github.com/jarun/advcpmv
Source0:        https://ftp.gnu.org/gnu/coreutils/coreutils-%{COREUTILS_VER}.tar.xz

Patch0:         advcpmv-0.9-%{COREUTILS_VER}.patch

BuildRequires:  gcc
BuildRequires:  make

%description
Advanced Copy is a mod for the GNU cp and GNU mv tools which adds a progress bar
and provides some info on what's going on. It was written by Florian Zwicke and
released under the GPL.


%prep
%setup -qn coreutils-%COREUTILS_VER
%autopatch -p1 -v


%build
%configure
%make_build


%install
gzip -k man/cp.1 man/mv.1
install -Dm 644 man/cp.1.gz %{buildroot}/%{_mandir}/man1/cpg.1.gz
install -Dm 644 man/mv.1.gz %{buildroot}/%{_mandir}/man1/mvg.1.gz
install -Dm 755 src/cp %{buildroot}/%{_bindir}/cpg
install -Dm 755 src/mv %{buildroot}/%{_bindir}/mvg


%files
%{_bindir}/cpg
%{_bindir}/mvg
%{_mandir}/man1/cpg.1.gz
%{_mandir}/man1/mvg.1.gz
%license COPYING
