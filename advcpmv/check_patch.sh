#!/usr/bin/env bash
#
set -euo pipefail

curl \
    --location \
    http://ftp.gnu.org/gnu/coreutils/coreutils-9.5.tar.xz |
    tar xJ

curl \
    --location \
    --output upstream.patch \
    https://raw.githubusercontent.com/jarun/advcpmv/refs/heads/master/advcpmv-0.9-9.5.patch

cp -r coreutils-* coreutils

cd coreutils-*
patch -p1 -i ../upstream.patch
cd ..

find . -iname "*.orig" -delete
diff -ruN coreutils coreutils-* >new.patch
