# https://github.com/cri-o/cri-o
%global goipath         github.com/cri-o/cri-o
%global gomodulesmode   GO111MODULE=on
Version:        1.31.1
%gometa
%bcond_without check

Name:           cri-o
Epoch:          0
Release:        1%{?dist}
Summary:        Open Container Initiative-based implementation of Kubernetes Container Runtime Interface
License:        Apache-2.0
URL:            https://github.com/cri-o/cri-o
Source0:        %url/archive/v%{version}/%{name}-%{version}.tar.gz
BuildRequires:  glib2-devel
BuildRequires:  glibc-static
BuildRequires:  go-md2man
BuildRequires:  golang >= 1.21
BuildRequires:  gpgme-devel
BuildRequires:  libassuan-devel
BuildRequires:  libseccomp-devel
BuildRequires:  make
BuildRequires:  systemd-rpm-macros
Requires:       container-selinux
Requires:       containers-common
Requires:       containernetworking-plugins
Requires:       runc
Requires:       conmon
Requires:       socat

%description
%{summary}.

%prep
%goprep -k

sed -i 's/install.config: crio.conf/install.config:/' Makefile
sed -i 's/install.bin: binaries/install.bin:/' Makefile
sed -i 's/install.man: $(MANPAGES)/install.man:/' Makefile
sed -i 's/\.gopathok //' Makefile
sed -i 's/module_/module-/' internal/version/version.go
sed -i 's/\/local//' contrib/systemd/crio.service
sed -i 's/\/local//' contrib/systemd/crio-wipe.service

%build
export GOFLAGS="-mod=vendor"

export BUILDTAGS="$(hack/btrfs_installed_tag.sh)
$(hack/btrfs_tag.sh) $(hack/libdm_installed.sh)
$(hack/libdm_no_deferred_remove_tag.sh)
$(hack/seccomp_tag.sh)
$(hack/selinux_tag.sh)"

for cmd in cmd/* ; do
    %gobuild -o bin/$(basename $cmd) %{goipath}/$cmd
done

export CFLAGS="$CFLAGS -std=c99"
%make_build bin/pinns
GO_MD2MAN=go-md2man make docs

%install
sed -i 's/\/local//' contrib/systemd/crio.service
bin/crio \
      --selinux \
      --cni-plugin-dir /opt/cni/bin \
      --cni-plugin-dir "%{_libexecdir}/cni" \
      --enable-metrics \
      --metrics-port 9537 \
      config > crio.conf

# install binaries
install -dp %{buildroot}{%{_bindir},%{_libexecdir}/crio}
install -p -m 755 bin/crio %{buildroot}%{_bindir}

# install conf files
install -dp %{buildroot}%{_sysconfdir}/cni/net.d
install -p -m 644 contrib/cni/10-crio-bridge.conflist %{buildroot}%{_sysconfdir}/cni/net.d/100-crio-bridge.conflist
install -p -m 644 contrib/cni/99-loopback.conflist %{buildroot}%{_sysconfdir}/cni/net.d/200-loopback.conflist

install -dp %{buildroot}%{_sysconfdir}/crio
install -dp %{buildroot}%{_datadir}/containers/oci/hooks.d
install -dp %{buildroot}%{_datadir}/oci-umount/oci-umount.d
install -p -m 644 crio.conf %{buildroot}%{_sysconfdir}/crio
#install -p -m 644 seccomp.json %%{buildroot}%%{_sysconfdir}/%crio
install -p -m 644 crio-umount.conf %{buildroot}%{_datadir}/oci-umount/oci-umount.d/crio-umount.conf
install -p -m 644 crictl.yaml %{buildroot}%{_sysconfdir}

%make_install PREFIX=%{buildroot}%{_prefix} \
    install.bin \
    install.completions \
    install.config \
    install.man \
    install.systemd

install -dp %{buildroot}%{_sharedstatedir}/containers

%post
%systemd_post crio

%preun
%systemd_preun crio

%postun
%systemd_postun_with_restart crio

%files
%license LICENSE
%doc docs code-of-conduct.md tutorial.md ADOPTERS.md CONTRIBUTING.md README.md
%doc awesome.md transfer.md
%{_bindir}/crio
%{_bindir}/pinns
%{_mandir}/man5/crio.conf*5*
%{_mandir}/man8/crio*.8*
%dir %{_sysconfdir}/crio
%config(noreplace) %{_sysconfdir}/crio/crio.conf
%config(noreplace) %{_sysconfdir}/cni/net.d/100-crio-bridge.conflist
%config(noreplace) %{_sysconfdir}/cni/net.d/200-loopback.conflist
%config(noreplace) %{_sysconfdir}/crictl.yaml
%dir %{_libexecdir}/crio
%{_unitdir}/crio.service
%{_unitdir}/crio-wipe.service
%dir %{_sharedstatedir}/containers
%dir %{_datadir}/containers
%dir %{_datadir}/containers/oci
%dir %{_datadir}/containers/oci/hooks.d
%dir %{_datadir}/oci-umount
%dir %{_datadir}/oci-umount/oci-umount.d
%{_datadir}/oci-umount/oci-umount.d/crio-umount.conf
%{_datadir}/bash-completion/completions/crio*
%{_datadir}/fish/completions/crio*.fish
%{_datadir}/zsh/site-functions/_crio*
