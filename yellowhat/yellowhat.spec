%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           yellowhat
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Yellowhat metapackage
License:        AGPL 3.0
URL:            https://gitlab.com/yellowhat/copr
BuildArch:      noarch

Requires:       alsa-sof-firmware
Requires:       wireplumber pipewire pipewire-pulseaudio
Requires:       xdg-desktop-portal-wlr xdg-desktop-portal-gtk
Requires:       NetworkManager-wifi wireguard-tools openssl openvpn
Requires:       fzf git-core btop neovim tree tar unzip fish powertop
Requires:       advcpmv bluez ntfs-3g rsync sf
Requires:       flatpak fwupd podman make
Requires:       sway grim slurp foot statusbar warpd wl-clipboard
Requires:       zathura zathura-pdf-mupdf
Requires:       liberation-sans-fonts
Requires:       joypixels-fonts comic-mono-fonts
Requires:       google-noto-emoji-color-fonts
Requires:       google-noto-sans-symbols-fonts
Requires:       google-noto-sans-symbols2-fonts
Requires:       oathtool


%description
Yellowhat metapackage


%prep


%build


%install


%files
