%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}
%global debug_package %{nil}

Name:           sf
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A simple console file manager written in python
License:        GPL 3.0
URL:            https://gitlab.com/yellowhat-labs/sf
Source0:        https://gitlab.com/yellowhat-labs/sf/-/raw/main/sf.py

%description
A simple console file manager written in python

%prep
%setup -cT
cp %{SOURCE0} sf


%install
mkdir -p %{buildroot}/%{_bindir}
install -m 755 -t %{buildroot}/%{_bindir} sf


%files
%{_bindir}/sf
