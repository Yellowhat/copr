# copr

Collection of `.spec` files  for automate build on **copr**:

* [yellowhat/yellowhat](https://copr.fedorainfracloud.org/coprs/yellowhat/yellowhat)
* [yellowhat/mule](https://copr.fedorainfracloud.org/coprs/yellowhat/mule)

## Build locally

```bash
podman run -it --rm -v "$PWD:/data:z" -w /data fedora:41

dnf install -y dnf-plugins-core rpmdevtools
spectool -g -R advcpmv.spec

dnf builddep -y advcpmv.spec
cp *.patch /root/rpmbuild/SOURCES/
rpmbuild -ba -vv advcpmv.spec
```
