%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           warpd
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A modal keyboard driven interface for mouse manipulation
License:        MIT
URL:            https://github.com/rvaiya/warpd
Source0:        %{url}/archive/master.tar.gz
BuildRequires:  gcc
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig(xkbcommon)


%description
A modal keyboard driven interface for mouse manipulation


%prep
%autosetup -v -n %{name}-master


%build
DISABLE_X=1 make


%install
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_mandir}/man1
install -m 644 -t %{buildroot}/%{_mandir}/man1 files/warpd.1.gz
install -m 755 -t %{buildroot}/%{_bindir} bin/warpd


%files
%license LICENSE
%doc README.md
%{_mandir}/man1/*
%{_bindir}/warpd
