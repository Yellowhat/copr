Name:           pdf.js
Version:        2.13.216
Release:        0%{?dist}
Summary:        PDF reader in javascript
License:        Apache
URL:            https://mozilla.github.io/pdf.js
Source0:        https://github.com/mozilla/pdf.js/releases/download/v%{version}/pdfjs-%{version}-dist.zip


%description
PDF reader in javascript


%prep
unzip %{SOURCE0}


%install
mkdir -p %{buildroot}/usr/share/pdf.js
cp -R build web %{buildroot}/usr/share/pdf.js/.
find %{buildroot}/usr/share/pdf.js/ -type f -exec chmod 644 {} \;


%files
%license LICENSE
/usr/share/pdf.js/
