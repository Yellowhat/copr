%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           wlsunset
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Day/night gamma adjustments for Wayland
License:        MIT
URL:            https://git.sr.ht/~kennylevinsen/wlsunset
Source0:        %{url}/archive/master.tar.gz
BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(scdoc)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-scanner)


%description
Day/night gamma adjustments for Wayland


%prep
%autosetup -v -n %{name}-master


%build
%meson
%meson_build


%install
%meson_install


%files
%license LICENSE
%doc README.md
%{_mandir}/man1/*
%{_bindir}/wlsunset
