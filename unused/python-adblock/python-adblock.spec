%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}
%global debug_package %{nil}

Name:           python-adblock
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Python wrapper for Brave's adblocking library.
License:        MIT and ASL 2.0
URL:            https://github.com/ArniDagur/python-adblock
Source0:        https://github.com/ArniDagur/%{name}/archive/master.zip
BuildRequires:  maturin
BuildRequires:  rust-packaging
BuildRequires:  python3-devel
BuildRequires:  unzip


%description
Python wrapper for Brave's adblocking library.


%prep
%autosetup -n %{name}-master


%build
maturin build --release --strip
unzip -d target/wheels/ target/wheels/*.whl


%install
mkdir -p %{buildroot}/%{python3_sitelib}/
cp -r target/wheels/adblock %{buildroot}/%{python3_sitelib}/
cp -r target/wheels/*.dist-info %{buildroot}/%{python3_sitelib}/


%files
%license LICENSE-MIT LICENSE-APACHE
%doc README.md CHANGELOG.md
%{python3_sitelib}/*.dist-info/
%{python3_sitelib}/adblock/
