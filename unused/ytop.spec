%global debug_package %{nil}

Name:           ytop
Version:        0.6.2
Release:        1%{?dist}
Summary:        TUI system monitor written in Rust
License:        MIT
URL:            https://github.com/cjbassi/ytop
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
BuildRequires:  cargo
BuildRequires:  rust

%description
Another TUI based system monitor, this time in Rust!


%prep
%autosetup -p1


%install
cargo install --root=%{buildroot}%{_prefix} --path=.
rm -f   %{buildroot}%{_prefix}/.crates.toml \
        %{buildroot}%{_prefix}/.crates2.json
strip --strip-all %{buildroot}%{_bindir}/*


%files
%license LICENSE
%doc README.md CHANGELOG.md
%{_bindir}/%{name}
