#!/usr/bin/env bash
set -euo pipefail

podman run -it --rm --privileged -v "$PWD:/data" fedora

dnf install -y git mock rpmdevtools rpmlint
rpmdev-setuptree
cd ~/rpmbuild

cp /data/sway-git.spec SPECS/
curl -L --output SOURCES/master.zip https://github.com/swaywm/sway/archive/master.zip
curl -L --output SOURCES/wlroots-master.zip https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/master/wlroots-master.zip
cp /data/sway*.patch SOURCES

rpmbuild -bs SPECS/sway-git.spec

mock -r fedora-35-x86_64 --rebuild SRPMS/sway-git-*.src.rpm
# /var/lib/mock/fedora-35-x86_64/root/builddir/
