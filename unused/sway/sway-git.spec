%global forgeurl  https://github.com/swaywm/sway
%global commit    37e4a3d6370dc6ba2b0877d588845c06781e880e
# dcd2076f3854f4bb0018b6a47781dc48a55393b0
%global date      20221127

%global forgeurl1 https://gitlab.freedesktop.org/wlroots/wlroots
%global commit1   35a0d9c85d63344c41a4b2fd5ec9ed4636889b8a
# 1712a7d27444d62f8da8eeedf0840b386a810e96
%global version1  0.17

%forgemeta -a
%undefine  distprefix0
%undefine  distprefix1

Name:           sway-git
Version:        %{date}
Release:        0%{?dist}
Summary:        i3-compatible window manager for Wayland
License:        MIT
URL:            https://swaywm.org
# Source0:        https://github.com/swaywm/sway/archive/master.zip
# Source1:        https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/master/wlroots-master.zip
Source0:        %{forgesource0}
Source1:        %{forgesource1}
# sway-git patches: build wlroots as static library and rename binaries
Patch0:         sway-git_binary-names.patch
# Don't require libdrm 2.4.112
# Patch1:         wlroots-revert-libdrm-bump.patch
# Fix nvidia screenshare
# Patch2:         sway-git_no-axrgb-assert.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  meson >= 0.60.0
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(gbm)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(glesv2)
BuildRequires:  pkgconfig(json-c)
BuildRequires:  pkgconfig(libdrm)
BuildRequires:  pkgconfig(libevdev)
BuildRequires:  pkgconfig(libinput)
BuildRequires:  pkgconfig(libpcre)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(pangocairo)
BuildRequires:  pkgconfig(pixman-1)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-cursor)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-scanner)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  glslang
BuildRequires:  pkgconfig(hwdata)
BuildRequires:  pkgconfig(libseat)
BuildRequires:  pkgconfig(vulkan)
# BuildRequires:  pkgconfig(x11-xcb)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-renderutil)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(xwayland)
Requires:       sway


%description
Sway is a tiling window manager supporting Wayland compositor protocol and
i3-compatible configuration.


%prep
# %setup -q -n sway-master -a 1
%setup -q -n %{extractdir0} -a 1
mkdir -p subprojects
# mv wlroots-master subprojects/wlroots
mv %{extractdir1} subprojects/wlroots
%autopatch -p1


%build
%meson \
    -Dwlroots:default_library=static  \
    -Dwlroots:examples=false          \
    -Dwlroots:xcb-errors=disabled     \
    -Dsd-bus-provider=libsystemd      \
    -Dbash-completions=false  \
    -Dfish-completions=false  \
    -Dzsh-completions=false   \
    -Ddefault-wallpaper=false \
    -Dman-pages=disabled      \
    -Dwerror=false

%meson_build


%install
%meson_install --skip-subprojects
# will use config from sway package
rm -rf %{buildroot}/%{_sysconfdir}
# will use utilities from sway package
rm -f %{buildroot}/%{_bindir}/{swaymsg,swaynag}


%files
%license LICENSE
%doc README.md
%{_bindir}/sway-git
%{_bindir}/swaybar-git
%{_datadir}/wayland-sessions/sway-git.desktop
