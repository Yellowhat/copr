Name:	        pixman
Version:        0.42.2
Release:        1%{?dist}
Summary:        Pixel manipulation library
License:        MIT
URL:            https://gitlab.freedesktop.org/pixman/pixman
Source:         https://xorg.freedesktop.org/archive/individual/lib/%{name}-%{version}.tar.xz

BuildRequires:  gcc
BuildRequires:  meson


%description
Pixman is a pixel manipulation library for X and Cairo.


%package devel
Summary:  Pixel manipulation library development package
Requires: %{name}%{?isa} = %{version}-%{release}
Requires: pkgconfig


%description devel
Development library for pixman.


%prep
%autosetup -p1


%build
%meson --auto-features=auto
%meson_build


%install
%meson_install


%check
%meson_test

%ldconfig_post
%ldconfig_postun


%files
%doc COPYING
%{_libdir}/libpixman-1*.so.*


%files devel
%dir %{_includedir}/pixman-1
%{_includedir}/pixman-1/pixman.h
%{_includedir}/pixman-1/pixman-version.h
%{_libdir}/libpixman-1*.so
%{_libdir}/pkgconfig/pixman-1.pc
