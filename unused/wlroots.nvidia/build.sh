#!/usr/bin/env bash
set -euo pipefail

podman run -it --rm --privileged -v "$PWD:/data" fedora

dnf install -y git mock rpmdevtools rpmlint
rpmdev-setuptree
cd ~/rpmbuild

cp /data/wlroots.spec SPECS/
curl -L --output SOURCES/wlroots-0.15.1.tar.gz https://gitlab.freedesktop.org/wlroots/wlroots/-/releases/0.15.1/downloads/wlroots-0.15.1.tar.gz
curl -L --output SOURCES/wlroots-0.15.1.tar.gz.sig https://gitlab.freedesktop.org/wlroots/wlroots/-/releases/0.15.1/downloads/wlroots-0.15.1.tar.gz.sig
curl -L --output SOURCES/gpgkey-0FDE7BE0E88F5E48.gpg https://emersion.fr/.well-known/openpgpkey/hu/dj3498u4hyyarh35rkjfnghbjxug6b19#/gpgkey-0FDE7BE0E88F5E48.gpg
cp /data/examples.meson.build SOURCES
cp /data/*.patch SOURCES

rpmbuild -bs SPECS/wlroots.spec

mock -r fedora-36-x86_64 --rebuild SRPMS/wlroots-*.src.rpm
# /var/lib/mock/fedora-36-x86_64/root/builddir/
