%global debug_package %{nil}

Name:           zenith
Version:        0.9.2
Release:        1%{?dist}
Summary:        TUI system monitor written in Rust
License:        MIT
URL:            https://github.com/bvaisvil/zenith
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
BuildRequires:  cargo
BuildRequires:  rust

%description
In terminal graphical metrics for your *nix system written in Rust


%prep
%autosetup -p1


%install
cargo install --root=%{buildroot}%{_prefix} --path=.
rm -f   %{buildroot}%{_prefix}/.crates.toml \
        %{buildroot}%{_prefix}/.crates2.json
strip --strip-all %{buildroot}%{_bindir}/*


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
