%global srcname bemenu
%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           %{srcname}
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Dynamic menu library and client program inspired by dmenu
License:        GPLv3+ and LGPLv3+
URL:            https://github.com/Cloudef/%{name}
Source0:        %{url}/archive/master.zip
Patch0:         bemenu_respect-env-build-flags.patch
BuildRequires:  gnupg2
BuildRequires:  gcc
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(ncursesw)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(pangocairo)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xinerama)
BuildRequires:  pkgconfig(xkbcommon)

%description
%{summary}.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for extending %{name}.


%prep
%autosetup -n %{name}-master -p1


%build
%make_build   PREFIX='%{_prefix}' libdir='/%{_lib}'


%install
%make_install PREFIX='%{_prefix}' libdir='/%{_lib}'


%files
%doc README.md
%license LICENSE-CLIENT LICENSE-LIB
%{_bindir}/%{name}
%{_bindir}/%{name}-run
%{_mandir}/man1/%{name}*.1*
# Long live escaping! %%%% resolves to %%; ${v%%.*} strips everything after first dot
%{_libdir}/lib%{name}.so*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/%{name}-renderer-curses.so
%{_libdir}/%{name}/%{name}-renderer-wayland.so
%{_libdir}/%{name}/%{name}-renderer-x11.so

%files devel
%doc README.md
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.so
%dir %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/%{name}.pc
