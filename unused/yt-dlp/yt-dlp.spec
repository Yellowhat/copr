%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           yt-dlp
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A small command-line program to download online videos
License:        Unlicense
URL:            https://github.com/yt-dlp/yt-dlp
Source0:        %{url}/archive/master.zip
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  %{py3_dist setuptools}
BuildRequires:  %{py3_dist pytest}
# Needed to query completion locations and to make docs
BuildRequires:  bash-completion
BuildRequires:  fish
BuildRequires:  pandoc
Requires:       %{py3_dist setuptools}
Recommends:     %{py3_dist pycryptodomex} %{py3_dist keyring}
BuildArch:      noarch

%description
Small command-line program to download videos from YouTube and other sites. It's a fork of youtube-dl(c).


%prep
%autosetup -v -n %{name}-master

cp -a setup.py setup.py.installpath
mv -f CONTRIBUTORS AUTHORS
# Remove pycryptodomex requirement (just recommend)
sed -i "s|, 'pycryptodomex'||" setup.py
sed -i '/pycryptodomex/d' requirements.txt
# Remove README.txt
sed -i '/README.txt/d' setup.py

mkdir -p completions/{bash,zsh,fish}
%{python3} devscripts/bash-completion.py
%{python3} devscripts/fish-completion.py
%{python3} devscripts/zsh-completion.py

# make docs (from Makefile)
MARKDOWN=$(if [ `pandoc -v | head -n1 | cut -d" " -f2 | head -c1` = "2" ]; then echo markdown-smart; else echo markdown; fi)
#
%{python3} devscripts/prepare_manpage.py %{name}.1.temp
pandoc -s -f "$MARKDOWN" -t man %{name}.1.temp -o %{name}.1
#
mv -f README.md README.md.temp
pandoc -f "$MARKDOWN" -t plain README.md.temp -o README.md
#
mv -f Changelog.md Changelog.md.temp
pandoc -f "$MARKDOWN" -t plain Changelog.md.temp -o Changelog.md
#
rm -f %{name}.1.temp README.md.temp Changelog.md.temp supportedsites.md
%{python3} devscripts/make_supportedsites.py supportedsites.md

# Remove interpreter shebang from module files
find yt_dlp -type f -exec sed -i -e '1{/^\#!\/usr\/bin\/env python$/d;};' {} +


%build
%py3_build


%install
%py3_install

# Remove test stuff
if [[ -d "%{buildroot}%{python3_sitelib}/test" ]]; then
    rm -r %{buildroot}%{python3_sitelib}/test
fi

mkdir -p %{buildroot}%{_sysconfdir}
cat > %{buildroot}%{_sysconfdir}/%{name}.conf << EOF
# Default configuration for yt-dlp.

--prefer-free-formats
--hls-prefer-native
EOF

# Check completion locations
cd %{buildroot}$(pkg-config --variable completionsdir bash-completion)
if ! [ -f "%{name}" ]; then exit 1; fi
cd %{buildroot}$(pkg-config --variable completionsdir fish)
if ! [ -f "%{name}.fish" ]; then exit 1; fi


%check
# make offlinetest
%pytest -k "not download"


%files
%doc AUTHORS Changelog.md README.md supportedsites.md
%{python3_sitelib}/yt_dlp/
%{python3_sitelib}/yt_dlp*.egg-info
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*
%config(noreplace) %{_sysconfdir}/%{name}.conf
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%{_datadir}/zsh/site-functions/_%{name}
