Name:          maturin
Version:       0.12.17
Release:       0%{?dist}
Summary:       Build tool for publishing rust as python packages
License:       MIT and ASL 2.0
URL:           https://github.com/PyO3/maturin
Source0:       %pypi_source %{name}
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-toml
BuildRequires: python3-tomli
BuildRequires: rust-packaging


%description
Build and publish crates with pyo3, rust-cpython and cffi bindings as well as
rust binaries as python packages.


%prep
%autosetup


%build
%py3_build


%install
%py3_install


%files
%license license-mit license-apache
%doc Readme.md Changelog.md
%{_bindir}/%{name}
%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info
%{python3_sitelib}/%{name}
