%global srcname kitty
%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           %{srcname}
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A modern, hack-able, feature full, OpenGL-based terminal emulator
License:        GPLv3
URL:            https://github.com/kovidgoyal/%{name}
Source0:        %{url}/archive/master.zip
BuildRequires:  dbus-devel
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  gcc
BuildRequires:  harfbuzz-devel
BuildRequires:  lcms2
BuildRequires:  lcms2-devel
BuildRequires:  libcanberra-devel
BuildRequires:  libpng-devel
BuildRequires:  libxkbcommon-x11-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXi-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  pkg-config
BuildRequires:  python3-devel
BuildRequires:  python3-sphinx
BuildRequires:  wayland-protocols-devel
BuildRequires:  zlib
Requires:       fontconfig
Requires:       freetype
Requires:       harfbuzz
Requires:       libcanberra
Requires:       libpng

%description
A cross-platform, fast, feature full, GPU based terminal emulator

%prep
%autosetup -v -n %{srcname}-master 

%build
python3 setup.py --verbose linux-package --debug --libdir-name %{_lib}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p %{buildroot}/usr
cp -r linux-package/* %{buildroot}/usr

%check

%files
%doc %{_mandir}/man1/kitty.1.gz
%doc CHANGELOG.rst 
%doc README.asciidoc
%doc %{_docdir}/kitty
%license LICENSE
%{_bindir}/kitty
%{_libdir}/kitty/*
%{_datadir}/applications/kitty.desktop
%{_datadir}/terminfo/x/xterm-kitty
%{_datadir}/icons/hicolor/256x256/apps/kitty.png

