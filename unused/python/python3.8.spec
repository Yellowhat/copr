%define major_version 3.8
%define minor_version 10
%define __requires_exclude /usr/local/bin/python
%define __os_install_post %{nil}

Name:    python%{major_version}
Version: %{major_version}.%{minor_version}
Release: 1%{?dist}
Summary: Interpreter of the Python programming language
License: Python
URL:     https://www.python.org
Source:  https://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz
Patch0:  lib64_py%{major_version}.patch

BuildRequires: bzip2-devel
BuildRequires: gcc gcc-c++
BuildRequires: libffi-devel
BuildRequires: make
BuildRequires: ncurses-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: readline-devel

%description
Interpreter of the Python programming language

%package devel
Summary: Libraries and header files needed for Python development
Requires: %{name} = %{version}-%{release}
%description devel
Libraries and header files needed for Python development

%package pip
Summary: A tool for installing and managing Python3 packages
Requires: %{name} = %{version}-%{release}
%description pip
A tool for installing and managing Python3 packages

%package tkinter
Summary: A GUI toolkit for Python
Requires: %{name} = %{version}-%{release}
%description tkinter
A GUI toolkit for Python

%package idle
Summary: A basic graphical development environment for Python
Requires: %{name} = %{version}-%{release}
%description idle
A basic graphical development environment for Python

%prep
%setup -c -n %{name}-%{version}
cd Python-%{version}
%if "%{_lib}" == "lib64"
%patch0 -p1
%endif

%build
cd Python-%{version}
%configure --enable-optimizations
%make_build build_all

%install
cd Python-%{version}
%make_install
rm %{buildroot}%{_mandir}/man1/python3.1*
rm %{buildroot}/usr/bin/2to3
rm %{buildroot}/usr/bin/idle3
rm %{buildroot}/usr/bin/pip3
rm %{buildroot}/usr/bin/pydoc3
rm %{buildroot}/usr/bin/python3
rm %{buildroot}/usr/bin/python3-config

%files
%{_bindir}/python3*
%{_bindir}/pydoc3*
%{_mandir}/man1/python3.*
%{_exec_prefix}/lib/python3*
%{_exec_prefix}/%{_lib}/python3*
%{_exec_prefix}/%{_lib}/libpython3*.a
%{_exec_prefix}/%{_lib}/pkgconfig/python*.pc
%exclude %{_exec_prefix}/lib/python3*/site-packages/pip
%exclude %{_exec_prefix}/lib/debug/usr/bin/python3*.debug
%exclude %{_exec_prefix}/%{_lib}/python3*/tkinter
%exclude %{_exec_prefix}/%{_lib}/python3*/idlelib

%files devel
%{_bindir}/2to3*
%{_includedir}/python3*/*

%files pip
%{_bindir}/pip3*
%{_exec_prefix}/lib/python3*/site-packages/pip

%files tkinter
%{_exec_prefix}/%{_lib}/python3*/tkinter

%files idle
%{_bindir}/idle3*
