%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           qutebrowser
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A keyboard-driven, vim-like browser based on PyQt5 and QtWebEngine
License:        GPLv3
URL:            https://www.qutebrowser.org
Source0:        https://github.com/%{name}/%{name}/archive/master.zip
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  asciidoc
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  python3-setuptools
Requires:       qt5-qtbase
Requires:       qt5-qtdeclarative
Requires:       python3-qt5
Requires:       python3-jinja2
Requires:       python3-PyYAML
Requires:       ((qt5-qtwebengine and python3-qt5-webengine) or (qt5-qtwebkit and python3-qt5-webkit))
Recommends:     (qt5-qtwebengine and python3-qt5-webengine and qt5-qtwebengine-devtools)
Recommends:     (qt5-qtwebkit and python3-qt5-webkit)
Recommends:     python3-pygments
Recommends:     python3-adblock

%description
qutebrowser is a keyboard-focused browser with a minimal GUI. Itâ€™s based on
Python, PyQt5 and QtWebEngine and free software, licensed under the GPL.
It was inspired by other browsers/addons like dwb and Vimperator/Pentadactyl.


%prep
%autosetup -n %{name}-master


%build
# Compile the man page
a2x -f manpage doc/qutebrowser.1.asciidoc
# Find all *.py files and if their first line is exactly '#!/usr/bin/env python3'
# then replace it with '#!/usr/bin/python3' (if it's the 1st line).
find . -type f -iname "*.py" -exec sed -i '1s_^#!/usr/bin/env python3$_#!/usr/bin/python3_' {} +
%py3_build


%install
%py3_install
# install .desktop file
desktop-file-install \
    --add-category="Network" \
    --delete-original \
    --dir=%{buildroot}%{_datadir}/applications \
    misc/org.qutebrowser.qutebrowser.desktop
# Install man page
install -Dm644 doc/%{name}.1 -t %{buildroot}%{_mandir}/man1
# Install icons
install -Dm644 icons/qutebrowser.svg -t "%{buildroot}%{_datadir}/icons/hicolor/scalable/apps"
for i in 16 24 32 48 64 128 256 512; do
    install -Dm644 "icons/qutebrowser-${i}x${i}.png" "%{buildroot}%{_datadir}/icons/hicolor/${i}x${i}/apps/qutebrowser.png"
done
# Set __main__.py as executable
chmod 755 %{buildroot}%{python3_sitelib}/%{name}/__main__.py
# Remove zero-length files:
# https://fedoraproject.org/wiki/Packaging_tricks#Zero_length_files
find %{buildroot} -size 0 -delete


%files
%license LICENSE
%doc README.asciidoc doc/changelog.asciidoc
%{python3_sitelib}/%{name}-*-py%{python3_version}.egg-info
%{python3_sitelib}/%{name}
%{_bindir}/%{name}
%{_datadir}/applications/org.qutebrowser.qutebrowser.desktop
%{_mandir}/man1/%{name}.1*
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/24x24/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_datadir}/icons/hicolor/512x512/apps/%{name}.png
