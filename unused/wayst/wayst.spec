%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}
%global debug_package %{nil}

Name:           wayst
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Simple terminal emulator for Wayland and X11 with OpenGL rendering and minimal dependencies.
License:        GPLv3
URL:            https://github.com/91861/wayst
Source0:        https://github.com/91861/wayst/archive/master.zip
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  freetype
BuildRequires:  fontconfig-devel
BuildRequires:  libxkbcommon-devel
BuildRequires:  libXrandr-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  wayland-devel
BuildRequires:  pkgconfig(libutf8proc)

%description
Simple terminal emulator for Wayland and X11 with OpenGL rendering and minimal dependencies.


%prep
%autosetup -n %{name}-master


%build
%make_build


%install
rm -rf %{buildroot}
install -Dm 755 wayst %{buildroot}/usr/bin/wayst


%files
/usr/bin/wayst
