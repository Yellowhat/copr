%global srcname dislocker
%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           %{srcname}
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Utility to access BitLocker encrypted volumes
License:        GPLv2+
URL:            https://github.com/Aorimn/%{name}
Source0:        %{url}/archive/master.zip
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
%if 0%{?fedora} || 0%{?rhel} >= 7
Requires:       ruby(release), ruby(runtime_executable)
%else
Requires:       %{_bindir}/ruby
%endif
Requires(post):  %{_sbindir}/update-alternatives
Requires(preun): %{_sbindir}/update-alternatives
Provides:        %{_bindir}/%{name}
BuildRequires:  gcc
BuildRequires:  cmake, mbedtls-devel, ruby-devel, %{_bindir}/ruby

%description
Dislocker has been designed to read BitLocker encrypted partitions ("drives")
under a Linux system. The driver has the capability to read/write partitions
encrypted using Microsoft Windows Vista, 7, 8, 8.1 and 10 (AES-CBC, AES-XTS,
128 or 256 bits, with or without the Elephant diffuser, encrypted partitions);
BitLocker-To-Go encrypted partitions (USB/FAT32 partitions).

The file name where the BitLocker encrypted partition will be decrypted needs
to be given. This may take a long time, depending on the size of the encrypted
partition. But afterward, once the partition is decrypted, the access to the
NTFS partition will be faster than with FUSE. Another thing to think about is
the size of the disk (same size as the volume that is tried to be decrypted).
Nevertheless, once the partition is decrypted, the file can be mounted as any
NTFS partition and won't have any link to the original BitLocker partition.

%package libs
Summary:         Libraries for applications using dislocker

%description libs
The dislocker-libs package provides the essential shared libraries for any
dislocker client program or interface.

%package -n fuse-dislocker
Summary:         FUSE filesystem to access BitLocker encrypted volumes
Provides:        %{_bindir}/%{name}
Provides:        dislocker-fuse = %{version}-%{release}
Provides:        dislocker-fuse%{?_isa} = %{version}-%{release}
Requires:        %{name}-libs%{?_isa} = %{version}-%{release}
Requires(post):  %{_sbindir}/update-alternatives
Requires(preun): %{_sbindir}/update-alternatives
BuildRequires:   fuse-devel

%description -n fuse-dislocker
Dislocker has been designed to read BitLocker encrypted partitions ("drives")
under a Linux system. The driver has the capability to read/write partitions
encrypted using Microsoft Windows Vista, 7, 8, 8.1 and 10 (AES-CBC, AES-XTS,
128 or 256 bits, with or without the Elephant diffuser, encrypted partitions);
BitLocker-To-Go encrypted partitions (USB/FAT32 partitions).

A mount point needs to be given to dislocker-fuse. Once keys are decrypted, a
file named 'dislocker-file' appears into this provided mount point. This file
is a virtual NTFS partition, it can be mounted as any NTFS partition and then
reading from it or writing to it is possible.

%prep
%autosetup -v -n %{name}-master 

%build
%cmake -D WARN_FLAGS="-Wall -Wno-error -Wextra" .
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
#make DESTDIR=$RPM_BUILD_ROOT install
%make_install

# Remove standard symlinks due to alternatives
rm -f $RPM_BUILD_ROOT{%{_bindir}/%{name},%{_mandir}/man1/%{name}.1*}

# Remove symlink, because of missing -devel package
rm -f $RPM_BUILD_ROOT%{_libdir}/libdislocker.so

# Clean up files for later usage in documentation
for file in *.md; do mv -f $file ${file%.md}; done
for file in *.txt; do mv -f $file ${file%.txt}; done

%post
%{_sbindir}/update-alternatives --install %{_bindir}/%{name} %{name} %{_bindir}/%{name}-file 60

%preun
if [ $1 = 0 ]; then
  %{_sbindir}/update-alternatives --remove %{name} %{_bindir}/%{name}-file
fi

%ldconfig_scriptlets libs

%post -n fuse-dislocker
%{_sbindir}/update-alternatives --install %{_bindir}/%{name} %{name} %{_bindir}/%{name}-fuse 80

%preun -n fuse-dislocker
if [ $1 = 0 ]; then
  %{_sbindir}/update-alternatives --remove %{name} %{_bindir}/%{name}-fuse
fi

%files
%{_bindir}/%{name}-bek
%{_bindir}/%{name}-file
%{_bindir}/%{name}-find
%{_bindir}/%{name}-metadata
%{_mandir}/man1/%{name}-file.1*
%{_mandir}/man1/%{name}-find.1*

%files libs
%{!?_licensedir:%global license %%doc}
%license LICENSE
%doc CHANGELOG README
%{_libdir}/libdislocker.so.*

%files -n fuse-dislocker
%{_bindir}/%{name}-fuse
%{_mandir}/man1/%{name}-fuse.1*
