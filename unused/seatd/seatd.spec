%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

%global libseat_sover   1
%global _hardened_build 1

# seatd is not as useful on Fedora. Disable it by default,
# but leave bcond for those who really wants to use it.
%bcond_with     server

Name:           seatd
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        Minimal seat management daemon
License:        MIT
URL:            https://github.com/kennylevinsen/%{name}
Source0:        %{url}/archive/master.zip
BuildRequires:  gcc
BuildRequires:  meson >= 0.53
BuildRequires:  pkgconfig(libsystemd)
%if %{with server}
BuildRequires:  pkgconfig(scdoc)
BuildRequires:  systemd-rpm-macros
%endif


%description
A seat management daemon, that does everything it needs to do.
Nothing more, nothing less. Depends only on libc.


%package -n     libseat
Summary:        Universal seat management library

%description -n libseat
A seat management library allowing applications to use whatever seat
management is available.

Supports:
 * seatd
 * (e)logind
 * embedded seatd for standalone operation

Each backend can be compile-time included and is runtime auto-detected or
manually selected with the LIBSEAT_BACKEND environment variable.

Which backend is in use is transparent to the application, providing a
simple common interface.


%package -n     libseat-devel
Summary:        Development files for libseat
Requires:       libseat%{?_isa} = %{version}-%{release}

%description -n libseat-devel
The libseat-devel package contains libraries and header files for
developing applications that use libseat.


%prep
%autosetup -v -p1 -n %{name}-master


%build
%global server_feature %{?with_server:enabled}%{!?with_server:disabled}
%meson \
    -Dlibseat-logind=systemd \
    -Dserver=%{server_feature}
%meson_build


%install
%meson_install

%if %{with server}
install -D -m 0644 -pv contrib/systemd/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
%endif


%check
%meson_test


%if %{with server}
%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun %{name}.service

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
# TODO: seatd-launch should run with elevated privileges, i.e. SUID or CAP_SETUID
%{_bindir}/%{name}-launch
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}-launch.1*
%{_sysusersdir}/%{name}.conf
%{_unitdir}/%{name}.service
%endif

%files -n libseat
%license LICENSE
%doc README.md
%{_libdir}/libseat.so.%{libseat_sover}

%files -n libseat-devel
%{_includedir}/libseat.h
%{_libdir}/libseat.so
%{_libdir}/pkgconfig/libseat.pc
