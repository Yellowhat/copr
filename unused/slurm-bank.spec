%global debug_package %{nil}

Name:           slurm-bank
Version:        1.4.2
Release:        0%{?dist}
Summary:        SLURM Bank, a collection of wrapper scripts to do banking
License:        GPLv2
URL:            https://jcftang.github.io/slurm-bank
Source0:        https://github.com/jcftang/%{name}/archive/master.zip
BuildRequires:  bash
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  rsync
Requires:       bash  
Requires:       perl
Requires:       slurm
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define path_settings HTMLDIR=%{_docdir}/%{name}-%{version}/html

%description
SLURM Bank, a collection of wrapper scripts for implementing full
resource allocation to replace Maui and GOLD.

With the scripts we are able to provide a simple banking system where we
can deposit hours to an account. Users are associated to these accounts
from which they can use to run jobs. If users do not have an account or
if they do not have hours in their account then they cannot run jobs.


%prep
%setup -q -n %{name}-master 


%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" \
	%{path_settings} \
	all %{?_with_docs: docs}


%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} \
	%{path_settings} \
	install %{?_with_docs: install-docs}
(find $RPM_BUILD_ROOT%{_bindir} -type f | sed -e s@^$RPM_BUILD_ROOT@@) > bin-man-doc-files

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d
install -m 644 src/sbank.bash_completion $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d/sbank

%clean
rm -rf %{buildroot}


%files -f bin-man-doc-files
%defattr(-,root,root,-)
%doc AUTHORS README
%{_mandir}/*
%{_sysconfdir}/bash_completion.d
%if %{?_with_docs:1}0
%doc html/*
%else
%doc doc/*
%endif
