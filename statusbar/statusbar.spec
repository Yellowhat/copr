%define build_timestamp %{lua: print(os.date("%Y.%m.%d_%H.%M"))}

Name:           statusbar
Version:        %{build_timestamp}
Release:        0%{?dist}
Summary:        A custom statusbar, written in rust
License:        AGPL 3.0
URL:            https://gitlab.com/yellowhat-labs/statusbar
Source0:        %{url}/-/archive/main/statusbar-main.zip
# Power from voltage-current (workaround for dell)
# Patch0:         power-va.patch
# Do not read power
# Patch0:         power-99.9.patch
BuildRequires:  rust-packaging


%description
A custom statusbar, written in rust.


%prep
%autosetup -v -n %{name}-main -p0
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires


%build
%cargo_build


%install
%cargo_install


%files
%{_bindir}/%{name}
